const fs = require("fs");

//fichier client
const filename = "./data/customers.json";


let rawdata = fs.readFileSync(filename, "utf8");
let objectClient = JSON.parse(rawdata);

const currentDate = new Date();



// const proc = require("process");


// require("dotenv").config();

let datalayer = {
    getAllCustomers: function () {
        //read json file
        const data = fs.readFileSync(filename);

        //parse to object
        const customers = JSON.parse(data);

        //return customers
        return customers;
    },

    getCustomers: function (number, page) {
        {


            //read json file
            let rawdata = fs.readFileSync(filename);

            //parse to object
            let customers = JSON.parse(rawdata);

            const total = customers.length;

            //filter by number and page
            if (number && page) {
                customers = customers.slice((page - 1) * number, page * number);
            }

            const result = {
                total: total,
                result: customers
            };

            return result;
        }
    },



    //ecriture nouveau customers


    addCustomers: function (customerJSON) {
        console.log("filename.length : ", filename.length);
        customerJSON.id = 101010;
        customerJSON.created_at = currentDate;
        objectClient.push(customerJSON);

        fs.writeFileSync(filename, JSON.stringify(objectClient), (error) => {
            if(error) throw error;
        });

        return customerJSON;
    }
};

module.exports = datalayer;
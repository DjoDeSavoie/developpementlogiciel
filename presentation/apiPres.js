var express = require("express");
const business = require("../business/business");
var app = express();


const apiServ = {
    start: function (port) {

        app.use(express.json());


    app.use(function (req, res, next) {

        res.header("Access-Control-Allow-Origin", "*");

        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

        next();

    });

        app.get("/test", function (req, res) { //get recoie qqc
            const testObj = {
                test: "test"
            };

            console.log("call done");
            res.json(testObj);
        });

        app.get("/api/customers", function (req, res) {

            const number = req.query.number;
            const page = req.query.page;

            // get customers from business layer
            // const customers = business.getAllCustomers();
            const resCustomers = business.getCustomers(number, page);

            // res.json(customers);
            res.json(resCustomers);
        });

        app.post("/api/customers", function (req, res) { //post envoie qqc

            const customerJSON = {
                "id": null, "email": req.body.email, "first": req.body.first, "last": req.body.last,
                "company": req.body.company, "created_at": null, "country": req.body.country
            };

            const newCustomer = business.addCustomers(customerJSON);

            res.json(newCustomer);

        })

        app.listen(port, function () {
            console.log("Server running on port " + port);
        });



    }
}

module.exports = apiServ;